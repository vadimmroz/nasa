import {useState} from 'react';
import classes from './registeer.module.css'
import axios from "axios";
import {Navigate, NavLink} from "react-router-dom";

const Register = ({setReg, setIsLogin, isLogin, setEditProfile}) => {
    const [input, setInput] = useState({
        name: '',
        pass: ''
    })
    const handleButton = () => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=login&name=' + input.name + '&pass=' + input.pass)
            .then(function (response) {
                if (!response.data) {
                    axios.get('https://galician-grooves.000webhostapp.com?funk=register&name=' + input.name + '&pass=' + input.pass)
                        .then(function (response) {
                           if(response.data === 'registered'){
                               localStorage.setItem('name', input.name)
                               localStorage.setItem('pass', input.pass)
                               setTimeout(()=>{
                                   setIsLogin(true)
                                   setEditProfile(true)
                               }, 500)
                           }
                        })
                }
            })

    }
    return (
        <>
            {isLogin && <Navigate to='/home'/>}
        <div className={classes.register}>
            <h1>Register</h1>
            <input type="text" onChange={e => {
                setInput({...input, name: e.target.value})
            }} placeholder='name'/>
            <input type="password" onChange={e => {
                setInput({...input, pass: e.target.value})
            }} placeholder='password'/>
            <div className={classes.buttons}>
                <button onClick={handleButton}>
                    Register
                </button>
                <button className={classes.reg} onClick={()=>setReg(true)}>Login</button>
            </div>
        </div>
            </>
    );
};

export default Register;