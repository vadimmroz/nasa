import React, {useEffect, useState} from 'react';
import classes from './project.module.css'
import {useParams} from "react-router-dom";
import axios from "axios";
import Footer from '../Footer/Footer.jsx'
import Modal from "../posts/components/modal/Modal.jsx";

const Project = () => {
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [post, setPost] = useState('')
    const {id} = useParams();

    const [data, setData] = useState(null)
    const [posts, setPosts] = useState(null)
    const handlePost = (index, help) => {
        setPost(posts[index])
        setIsModalVisible(help)
    }
    useEffect(() => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=project&name=' + id)
            .then(function (response) {
                if (response.data) {
                    setData(response.data)
                }
            })
    }, []);
    useEffect(() => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=posts&name=' + id)
            .then(function (response) {
                if (response.data) {
                    setPosts(response.data)
                }
            })
    }, [])

    return (
        <>

            {data &&

                <div className={classes.project}>
                    {/*<button>*/}
                    {/*    <img src="https://i.ibb.co/4S5h4P7/free-icon-like-12328742.png" alt=""/>*/}
                    {/*    */}
                    {/*</button>*/}
                    {isModalVisible && <Modal handlePost={handlePost} post={post}/>}
                    <h2>
                        {data[0][1]}
                    </h2>
                    <img src={data[0][10]} alt=""/>
                    <p className={classes.discription}>{data[0][6]}</p>
                    <iframe src={data[0][15]}></iframe>
                    <div className={classes.posts}>
                        {posts && posts.map((e, index) => {
                            return (<div className={classes.post} key={index} onClick={() => handlePost(index, true)}>
                                <img src={e[3]} alt="img"/>
                                <h4>{e[1]}</h4>
                            </div>)
                        })}
                    </div>
                </div>

            }
            <Footer/>
        </>
    );
};

export default Project;