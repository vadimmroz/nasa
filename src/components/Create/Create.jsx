import classes from './create.module.css'
import {useState} from "react";
import CrateProject from "./components/CreateProject/CrateProject.jsx";
import CreatePost from "./components/CreatePost/CreatePost.jsx";
import classNames from "classnames";
import CreatePostChange from "./components/createPostChange/CreatePostChange.jsx";

const Create = ({isCreateVisible}) => {
    const [isCreateProjectVisible, setIsCreateProjectVisible] = useState(false)
    const [isCreatePostChangeVisible, setIsCreatePostChangeVisible] = useState(false)
   const [isCreatePostVisible, setIsCreatePostVisible] = useState(false)
    const [help, setHelp] = useState("")
    const handleProject = () => {
        setIsCreateProjectVisible(!isCreateProjectVisible)
    }
    const handlePostChange = ()=>{
        setIsCreatePostChangeVisible(!isCreatePostChangeVisible)
    }
    const handlePost = (e) => {
        setIsCreatePostVisible(!isCreatePostVisible)
        setHelp(e)
    }
    return (
        <>
            {isCreateProjectVisible && <CrateProject handleProject={handleProject}/>}
            {isCreatePostVisible && <CreatePost help={help} handlePost={handlePost}/>}
            <div className={isCreateVisible ? classNames(classes.create, classes.start) : classNames(classes.create, classes.close) }>
                {isCreatePostChangeVisible && <CreatePostChange handleProject={handleProject} handlePost={handlePost}/>}
                <button onClick={handleProject}>
                    project
                </button>
                <button onClick={handlePostChange}>
                    post
                </button>
            </div>
        </>
    );
};

export default Create;