import classes from './createProject.module.css'
import {useEffect, useState} from "react";
import axios from "axios";


const CrateProject = ({handleProject}) => {
    const [user, setUser] = useState(null)
const [data, setData] = useState({
    name: '',
    date: new Date(),
    deadLine: '',
    img:'',
    description: '',
    gitLink: ''

})

    useEffect(() => {
        const start = () => {
            axios.get("https://galician-grooves.000webhostapp.com?funk=user&name=" + localStorage.getItem('name'))
                .then(function (response) {
                    if (response.data) {
                        setUser(response.data)
                    }
                })
        }
        start()
    }, []);
    const createProject = () => {
        axios.post("https://galician-grooves.000webhostapp.com?funk=createProject&name=" + data.name + "&author=" + localStorage.getItem('name') +
            "&date=" + data.date + "&deadLine=" + data.deadLine + "&img=" + data.img + "&discription=" + data.description + "&gitLink=" + data.gitLink
        )
            .then(function (response) {

            })
    }
    return (
        <div className={classes.createProject} onClick={handleProject}>
            <div className={classes.container} onClick={e => {
                e.stopPropagation();
                e.preventDefault()
            }}>
                <button className={classes.closed} onClick={handleProject}>
                    X
                </button>
                <h3>
                    Create Project
                </h3>
                <input placeholder='name' onChange={e => {
                    setData({...data, name: e.target.value})
                }}/>
                <textarea placeholder='discription' onChange={e => {
                    setData({...data, description: e.target.value})
                }}>

                </textarea>
                <input type="date" id="date" name="date" onChange={e => {
                    setData({...data, deadLine: e.target.value})
                }}/>
                <input type="text" placeholder='GitLink' onChange={e => {
                    setData({...data, gitLink: e.target.value})
                }}/>
                <input type="text" placeholder='image' onChange={e => {
                    setData({...data, img: e.target.value})
                }}/>

                <button onClick={createProject}>Create Project</button>
            </div>
        </div>
    );
};

export default CrateProject;