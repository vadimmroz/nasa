import React, {useEffect, useState} from 'react';
import classes from './createPostChange.module.css'
import axios from "axios";

const CreatePostChange = ({handlePost, handleProject}) => {
    const [projects, setProjects] = useState([])
    // const [name, setName] = useState("")


    useEffect(() => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=userProject&name=' + localStorage.getItem('name'))
            .then(function (response) {
                if (response.data) {
                    setProjects(response.data)
                }
            })
    }, [])

    const handle = (e) => {
        handlePost(e)
    }
    return (
        <div className={classes.createPostChange}>
            {projects && (projects.length > 0 ? projects.map((e, index) => {
                    return (

                        <div className={classes.project} key={index} onClick={() => handle(e[1])}>
                            <p>{e[1]}</p>
                        </div>

                    )
                }) :
                <div className={classes.haveNot}>
                    <p>You have not any projects. Create now?</p>
                    <button onClick={handleProject}>+</button>
                </div>)}
        </div>
    );
};

export default CreatePostChange;