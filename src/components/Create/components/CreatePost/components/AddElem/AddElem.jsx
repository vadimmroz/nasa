import React, {useEffect, useState} from 'react';
import classes from './addElem.module.css'
const AddElem = ({handleAdd, setAdd, setRegime,addComponents}) => {
    const [select, setSelect] = useState('Photo')
    useEffect(()=>{
        setRegime(select)
    },[select])

    return (
        <div className={classes.addElem} onClick={e=>{e.stopPropagation();e.preventDefault(); }}>
                <button onClick={handleAdd} className={classes.close}>
                    X
                </button>

                <select onChange={(e) => setSelect(e.target.value)}>
                    <option>
                        Photo
                    </option>
                    <option>
                        Text
                    </option>
                    <option>
                        Video
                    </option>
                </select>
                {select === "Photo" && <input onChange={e=>setAdd(e.target.value)} type='text' placeholder='Photo link'/>}
                {select === "Text" && <textarea onChange={e=>setAdd(e.target.value)}  placeholder=''/>}
                {select === "Video" && <input onChange={e=>setAdd(e.target.value)} type='text' placeholder='Iframe component from Youtube'/>}
                <button className={classes.add} onClick={()=>{handleAdd(); addComponents();}}>
                    +
                </button>
        </div>
    );
};

export default AddElem;