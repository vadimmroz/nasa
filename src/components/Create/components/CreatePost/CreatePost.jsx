import classes from './createPost.module.css'
import {useState, useEffect} from "react";
import AddElem from "./components/AddElem/AddElem.jsx";
import axios from "axios";
import posts from "../../../posts/Posts.jsx";


const CreatePost = ({handlePost, help}) => {
    const [add, setAdd] = useState('')
    const [regime, setRegime] = useState('')
    const [isAddElemVisible, setIsElemVisible] = useState(false)
    const [title, setTitle] = useState('')
    const [post, setPost] = useState(
        {
            name: '',
            discription: '',
            img: '',
            date: new Date(),
            body: '<h2></h2>',
            authorImg: ''
        }
    )
    const handleAdd = () => {
        setIsElemVisible(!isAddElemVisible)
    }
    useEffect(() => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=user&name=' + localStorage.getItem('name'))
            .then(function (response) {
                if (response.data) {
                    setPost({...post, authorImg: response.data[0][3]})
                }
            })
    }, [])
    const addComponents = () => {
        switch (regime) {
            case 'Photo':
                setPost({...post, body: post.body + " <img src='" + add + "' alt='photo' />"})
                break;
            case 'Text':
                setPost({...post, body: post.body + " <p>" + add + "</p>"})
                break;
            case 'Video':
                setPost({...post, body: post.body + " " + add})
        }
    }

    useEffect(() => {
        let a = '<h2>' + title + '</h2>'
        let k = ''
        for (let i = 0; i < post.body.length; i++) {
            if (post.body[i - 1] === '2' && post.body[i - 2] === 'h' && post.body[i - 3] === '/') {
                k = post.body.substring(i + 1, post.body.length)
            }
        }
        setPost({...post, body: a + k})
    }, [title])

    const addPost = () => {
        axios.post('https://galician-grooves.000webhostapp.com?funk=addPost&name=' + title + '&discription=' + post.discription + '&img='+ post.img + '&date=' + post.date + '&body=' + post.body + '&project=' + help + '&authorImg=' + post.authorImg + '&author=' + localStorage.getItem('name'))
    }

    return (
        <div className={classes.createPost} onClick={() => handlePost('')}>
            {isAddElemVisible &&
                <AddElem addComponents={addComponents} setRegime={setRegime} setAdd={setAdd} handleAdd={handleAdd}/>}
            <div className={classes.content} onClick={e => {
                e.stopPropagation();
                e.preventDefault()
            }}>
                <h3>Create Post</h3>

                <button className={classes.close} onClick={handlePost}>X</button>
                <input type="text" placeholder='name' onChange={e => {
                    setPost({...post, name: e.target.value})
                    setTitle(e.target.value)
                }}/>
                <textarea placeholder='description' onChange={e => setPost({...post, discription: e.target.value})}/>
                <input type="text" placeholder='img' onChange={e => setPost({...post, img: e.target.value})}/>
                <textarea value={post.body} onChange={e => {
                    setPost({...post, body: e.target.value})
                }} placeholder='body'></textarea>

                <div dangerouslySetInnerHTML={{__html: post.body}}>

                </div>

                <button className={classes.add} onClick={handleAdd}>
                    +
                </button>

                <button className={classes.create} onClick={addPost}>
                    Create
                </button>
            </div>
        </div>
    );
};

export default CreatePost;