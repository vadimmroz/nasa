// eslint-disable-next-line no-unused-vars
import React, {useEffect, useState} from 'react';
import classes from './login.module.css'
import axios from "axios";
import {Navigate, NavLink} from "react-router-dom";

const Login = ({setIsLogin, isLogin, setReg}) => {
    const [input, setInput] = useState({
        name: '',
        pass: ''
    })

    useEffect(() => {
        if (localStorage.getItem('name') && localStorage.getItem('pass')) {
            axios.get('https://galician-grooves.000webhostapp.com?funk=login&name=' + localStorage.getItem('name') + '&pass=' + localStorage.getItem('pass'))
                .then(function (response) {
                    if (response.data) {
                        setIsLogin(true)
                    }
                })
        }
    }, [])
    const handleButton = () => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=login&name=' + input.name + '&pass=' + input.pass)
            .then(function (response) {
                if (response.data) {
                    localStorage.setItem('name', input.name)
                    localStorage.setItem('pass', input.pass)

                    setTimeout(() => {
                        setIsLogin(true)
                    }, 500)
                }
            })


    }
    return (
        <>
            {isLogin && <Navigate to='/home'/>}

            <div className={classes.login}>
                <h1>Login</h1>
                <input type="text" onChange={e => {
                    setInput({...input, name: e.target.value})
                }} placeholder='name'/>
                <input type="password" onChange={e => {
                    setInput({...input, pass: e.target.value})
                }} placeholder='password'/>
                <div className={classes.buttons}>
                    <button onClick={handleButton}>
                        Login
                    </button>
                    <button className={classes.reg} onClick={()=>setReg(false)}>Register</button>
                </div>
            </div>
        </>
    );
};

export default Login;