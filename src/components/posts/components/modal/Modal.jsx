import React from 'react';
import classes from './modal.module.css'

const Modal = ({handlePost, post}) => {
    // console.log(post)
    return (
        <div className={classes.modal}>
            <button onClick={handlePost} className={classes.x}>
                X
            </button>
            <div className={classes.post} onClick={e=> {e.preventDefault(); e.stopPropagation()}} dangerouslySetInnerHTML={{__html: post[9]}}>

            </div>
        </div>
    );
};

export default Modal;