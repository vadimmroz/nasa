// eslint-disable-next-line no-unused-vars
import React, {useEffect, useState} from 'react';
import classes from './posts.module.css'
import axios from "axios";
import Modal from "./components/modal/Modal.jsx";
import Donate from "./components/Donate/Donate.jsx";
import Footer from "../Footer/Footer.jsx";

const Posts = () => {

    const [data, setData] = useState([])
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [isDonateVisible, setIsDonateVisible] = useState(false)
    const [post, setPost] = useState({})
    useEffect(() => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=posts')
            .then(function (response) {
                if (response.data) {
                    setData(response.data)
                }
            })
    }, [])
    const handlePost = (e) => {
        setPost(e)
        setIsModalVisible(!isModalVisible)
    }
    const handleDonate = ()=>{
        setIsDonateVisible(!isDonateVisible)
    }
    return (
        <>

            <div className={classes.posts}>
                {isModalVisible && <Modal handlePost={handlePost} post={post}/>}

                {data && data.map((e, i) => {

                    return (

                        <div className={classes.post} key={i} onClick={()=>handlePost(e)}>
                            {isDonateVisible && <Donate handleDonate={handleDonate} post={e}/>}

                            <div className={classes.body}>
                                <img src={e[3]} alt=""/>
                                <p>{e[2].length > 40 ? e[2].slice(0, 40) + '...' : e[2]}</p>

                                <div className={classes.postBtns}>
                                    <div className={classes.author}>
                                        <img src={e[13]} alt=""/>
                                    </div>
                                    <div className={classes.donate}>
                                        <img src="https://i.ibb.co/7QgSJFp/coin-stack-regular-24.png" alt=""/>
                                    </div>
                                    <div className={classes.container}>
                                        <div className={classes.like} onClick={(e) => {
                                            handleLike(e, index);
                                            e.preventDefault();
                                            e.stopPropagation();
                                            setTrigger(!trigger);
                                        }}>
                                            <img src="https://i.ibb.co/4S5h4P7/free-icon-like-12328742.png" alt=""/>

                                        </div>
                                        <div className={classes.likeCount}>
                                            {/*{JSON.parse(e[12]).length}*/}
                                        </div>
                                    </div>
                                    <div className={classes.vacansies}>
                                        <img src="https://i.ibb.co/7CW5vMS/briefcase-regular-24.png" alt=""/>
                                    </div>
                                    <div className={classes.comm} onClick={ e => {
                                        handleCommVisible();
                                        e.preventDefault();
                                        e.stopPropagation();
                                    }}>
                                        <img src="https://i.ibb.co/Jcznycs/message-square-dots-regular-24.png" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    )
                })}
            </div>
            <Footer/>
        </>
    );
};

export default Posts;