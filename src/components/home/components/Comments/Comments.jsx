import React, {useEffect, useState} from 'react';
import axios from "axios";
import classes from "./comments.module.css";

const Comments = ({commVisible, handleCommVisible}) => {
    const [data, setData] = useState(null)
    const [comment, setComment] = useState('')

    useEffect(() => {
        const start = async () => {
            axios.get('https://galician-grooves.000webhostapp.com?funk=projects')
                .then(function (response) {
                    if (response.data) {
                        setData(response.data)
                    }
                })
        }
        start()
    }, [])
    //
    // useEffect(() => {
    //     setData(data)
    // }, [trigger])

    const handleComment = (e, index) => {
        let a = JSON.parse(data[index][14])
        setComment(localStorage.getItem('name') + ':' + comment)
        a.push(comment)
        data[index][14] = JSON.stringify(a)
        setData(data)
        axios.post("https://galician-grooves.000webhostapp.com?funk=commentProject&project=" + data[index][0] + "&comments=" + data[index][14])
        setComment('')
    }

    return (
        <>
            <div
                className={commVisible ? (classes.comments + ' ' + classes.moveLeft) : (classes.comments + ' ' + classes.moveRight)}
                onClick={handleCommVisible}
            >
                <div className={classes.commentsList}>

                </div>
                <div className={classes.form}>
                    <input type="text" value={comment}
                           onChange={e => {
                               setComment(e.target.value)
                           }}
                           onClick={
                               e => {
                                   e.stopPropagation();
                                   e.preventDefault();
                               }
                           }
                           className={classes.text}
                    />
                    <button
                        onClick={(event) => {
                            handleComment(e, index);
                            event.preventDefault();
                            event.stopPropagation();
                        }}
                        className={classes.sendBtn}
                    >
                        send
                    </button>
                </div>
            </div>
        </>
    );
};

export default Comments;