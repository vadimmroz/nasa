// eslint-disable-next-line no-unused-vars
import React, {useEffect, useState, useTransition} from 'react';
import classes from './home.module.css'
import axios from "axios";
import {Link} from "react-router-dom";
// eslint-disable-next-line no-unused-vars
import Header from "../Header/Header.jsx";
import Footer from "../Footer/Footer.jsx";
import Comments from "./components/Comments/Comments.jsx";

const Home = () => {
    const [data, setData] = useState(null)
    const [trigger, setTrigger] = useState(false)
    const [commVisible, setCommVisisble] = useState(false)

    useEffect(() => {
        const start = async () => {
            axios.get('https://galician-grooves.000webhostapp.com?funk=projects')
                .then(function (response) {
                    if (response.data) {
                        setData(response.data)
                    }
                })
        }
        start()
    }, [])

    useEffect(() => {
        setData(data)
    }, [trigger])
    const handleLike = (e, index) => {
        // let liked = JSON.parse(e[12]).includes(localStorage.getItem('name'))
        let a = JSON.parse(data[index][12])
        if (a.includes(localStorage.getItem('name'))) {
            let b = []
            a.forEach((element) => {
                if (element != localStorage.getItem('name')) {
                    b.push(element)
                }
            })
            a = b
        } else {
            a.push(localStorage.getItem('name'))
        }
        data[index][12] = JSON.stringify(a)
        setData(data)
        axios.post("https://galician-grooves.000webhostapp.com?funk=likeProject&project=" + data[index][0] + "&like=" + data[index][12])
    }

    const handleCommVisible = () => {
        setCommVisisble(!commVisible)
    }


    return (
        <>

            <div className={classes.home}>
                {data && data.map((e, index) => {
                    return (
                        <div key={index} className={classes.post}>
                            <Link to={'/' + e[1]} className={classes.content}>
                                <h3>{e[1]}</h3>
                                <img src={e[10]} alt=""/>
                                <p className={classes.date}>{e[3]}</p>
                                <div className={classes.discription}>
                                    <p>{e[6].length > 40 ? e[6].slice(0, 40) + '...' : e[6]}</p>
                                </div>
                                <div className={classes.postBtns}>
                                    <div className={classes.author}>
                                        <img src={e[13]} alt=""/>
                                    </div>
                                    <div className={classes.donate}>
                                        <img src="https://i.ibb.co/7QgSJFp/coin-stack-regular-24.png" alt=""/>
                                    </div>
                                    <div className={classes.container}>
                                        <div className={classes.like} onClick={(e) => {
                                            handleLike(e, index);
                                            e.preventDefault();
                                            e.stopPropagation();
                                            setTrigger(!trigger);
                                        }}>
                                            <img src="https://i.ibb.co/4S5h4P7/free-icon-like-12328742.png" alt=""/>

                                        </div>
                                        <div className={classes.likeCount}>
                                            {JSON.parse(e[12]).length}
                                        </div>
                                    </div>
                                    <div className={classes.vacansies}>
                                        <img src="https://i.ibb.co/7CW5vMS/briefcase-regular-24.png" alt=""/>
                                    </div>
                                    <div className={classes.comm} onClick={ e => {
                                        handleCommVisible();
                                        e.preventDefault();
                                        e.stopPropagation();
                                    }}>
                                        <img src="https://i.ibb.co/Jcznycs/message-square-dots-regular-24.png" alt=""/>
                                    </div>
                                </div>
                                <Comments commVisible={commVisible} handleCommVisible={handleCommVisible}/>
                            </Link>
                        </div>
                    )
                })
                }
            </div>
            <Footer/>
        </>
    );
};

export default Home;