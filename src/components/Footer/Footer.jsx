import {useState} from 'react';
import classes from './footer.module.css'
import {NavLink} from "react-router-dom";
import Create from "../Create/Create.jsx";

const Footer = () => {
    const [isCreateVisible, setIsCreateVisible] = useState(false)
    const handleCreate = () => {
        setIsCreateVisible(!isCreateVisible)
    }
    return (
        <>
            {isCreateVisible && <Create isCreateVisible={isCreateVisible}/>}
            <div className={classes.footer}>
                <NavLink to='/home'><img src={"https://i.ibb.co/gRryLTC/home-alt-2-regular-24.png"} alt=""/></NavLink>
                <NavLink to='/search'><img src={"https://i.ibb.co/K2Djwqy/search-regular-24-2.png"} alt=""/></NavLink>
                <button onClick={handleCreate}><img src={"https://i.ibb.co/v4896DW/add-to-queue-regular-24.png"}
                                                    alt=""/></button>
                <NavLink to='/direct'><img src={"https://i.ibb.co/3c5W5xK/chat-regular-24.png"} alt=""/></NavLink>
                <NavLink to='/profile'><img src={"https://i.ibb.co/89nFMZT/user-circle-regular-24-1.png"}
                                         alt=""/></NavLink>
            </div>
        </>
    );
};

export default Footer;