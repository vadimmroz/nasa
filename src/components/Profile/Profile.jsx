import React, {useEffect, useState} from 'react';
import classes from './profile.module.css'
import Footer from '../Footer/Footer.jsx'
import axios from "axios";
import {Link, Navigate} from "react-router-dom";


const Profile = ({setIsLogin, setEditProfile}) => {

    // eslint-disable-next-line no-unused-vars
    const [data, setData] = useState(null)
    const [projects, setProjects] = useState(null)
    const [logOut, setLogOut] = useState(false)

    useEffect(() => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=userProject&name=' + localStorage.getItem('name'))
            .then(function (response) {
                if (response.data) {
                    setProjects(response.data)
                }
            })
    }, [])

    useEffect(() => {
        axios.get('https://galician-grooves.000webhostapp.com?funk=user&name=' + localStorage.getItem('name'))
            .then(function (response) {
                if (response.data) {
                    setData(response.data)
                }
            })
    }, [])

    const handleLogOut = ()=>{
        localStorage.clear()
        setTimeout(()=>{
            setLogOut(true)
            setIsLogin(false)
        }, 500)

    }

    return (
        <>
            {logOut && <Navigate to='/login'/>}
            {data &&
                <div className={classes.profile}>
                    <div className={classes.miniProfile}>
                        <img src={data[0][3]} alt="" className={classes.userImg}/>
                        <div className={classes.userInfo}>
                            <h3>@{data[0][0]}</h3>
                            <div className={classes.buttons}>
                                <button className={classes.edit} onClick={()=>setEditProfile(true)}>Edit</button>
                                <button className={classes.logOut} onClick={handleLogOut}>Log Out</button>
                            </div>
                        </div>

                        <div dangerouslySetInnerHTML={{__html: data[0][4]}} className={classes.discription}></div>
                    </div>
                    <div className={classes.posts}>
                        {projects && projects.map((e, i) => {
                            return (
                                <Link to={"/" + e[1]} className={classes.post} key={i}>
                                    <h4>{e[1].length > 14 ? e[6].slice(0, 11) + '...' : e[1]}</h4>
                                    <img src={e[10]} alt=""/>
                                </Link>
                            )
                        })}
                    </div>

                    <Footer/>
                </div>
            }
        </>
    );
};

export default Profile;