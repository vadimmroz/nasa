import './App.css'
import {Routes, Route, Navigate} from "react-router-dom";
import Login from "./components/Login/Login.jsx";
import Register from "./components/Register/Register.jsx";
import Header from "./components/Header/Header.jsx";
import Profile from "./components/Profile/Profile.jsx";
import Project from "./components/project/Project.jsx";
import {useEffect, useState} from "react";
import EditProfile from "./components/EditProfile/EditProfile.jsx";
import Direct from "./components/direct/Direct.jsx";

function App() {
    const [isLogin, setIsLogin] = useState(false)
    const [editProfile, setEditProfile] = useState(false)

    const [reg, setReg] = useState(false)
    useEffect(()=>{
        if(localStorage.getItem('name') && localStorage.getItem('pass')){
            setIsLogin(true)
        }
        else {
            setIsLogin(false)
        }
    } , [])
  return (
    <>
        {!isLogin && <Navigate to='/login'/>}
        {editProfile && <EditProfile setEditProfile={setEditProfile}/>}
        <Routes>
            {/*<Route path='*' element={<Navigate to='/login'/>}/>*/}
            <Route path='/home' element={<Header/>}/>
            <Route path='/login' element={reg ? <Login setReg={setReg} setIsLogin={setIsLogin} isLogin={isLogin}/> : <Register setIsLogin={setIsLogin} isLogin={isLogin} setReg={setReg} setEditProfile={setEditProfile} />}/>
            <Route path='/profile' element={<Profile setEditProfile={setEditProfile} setIsLogin={setIsLogin} />}/>
            <Route path='/:id' element={<Project/>}/>
            <Route path='/direct' element={<Direct/>}/>
        </Routes>
    </>
  )
}

export default App
